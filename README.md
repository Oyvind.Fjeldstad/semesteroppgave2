*** Spelet er no satt opp slik eg ville hatt det om eg hadder spelt sjølv. For å gjere det enklare, eller vanskeligare, gå til Level.java i 'director'-mappa og gjer endringar der. No tar det ca. 15-20 min å runne spelet, så om man vil at det f.eks. skal gå raskare å teste spelet kan man då gå til Level.java og endre det der. <br><br>

# Speed Racer

Videolink: https://www.youtube.com/watch?v=uNXVSMqbU2s

"Speed Racer" er eit bilspel der ein oppgraderer bilen sin for å greie ulike level og til slutt bli verdsmeister. Spelet er strukturert slik at man køyrer ein "hotlap" og tener "ø-coins" som ein kan oppgradere bilen med. Inntekt av ø-coinsa blir kalkulert etter kor raskt ein køyre, og oppgraderingane blir eksponentielt dyrare utover i spelet. 

For å fullføre ein runde må bilen ha vore innom alle «checkpointa» undervegs. Om ein alltid er på bana vil dette gå greitt, om ikkje kan det hende ein må i retur for å hente opp igjen eit checkpoint.<br><br>

## Bana
Bana består av ulike underlag. Asfalt er asfaltfarga, grått. Gras er grønt, grus er beige. Det er også plassert «kerbs» i mange svinger. To viktige underlag å vere bevisst på er dei «gule trampolinane» og rosa, som symboliserer ein vegg. På dei gule trampolinane vil ein miste mykje fart om ein gassar når ein køyre over, og ein vil naturlegvis krasje om ein køyre inn i ein rosa vegg.

<img align="center" width=400 src="./resources/terrain.png">
<br><br>
Det er senterpunktet på bakakslinga som gjelder som posisjon. Så det er dette punktet som må passere gjennom checkpoint, og også bestemmer kva for underlag bilen køyre på.

<img align="center" width=400 src="./resources/center.png"><br><br>

## Kontroller
For å køyre bilen brukar man piltastane: <br>
 - «Opp» for å akselerere <br>
 - «Ned» for å bremse <br>
 - «Venstre» og «høgre» for å svinge til venstre og høgre <br>
Elles brukar ein musa til å navigere menyar og bruke knappar plassert på skjermen. <br><br>

## Ulike skjermer
På start skjermen kan man skrive inn namn på brukar og bil, ikkje obligatorisk.

<img align="center" width=400 src="./resources/start.png">
<br><br>
For å sjå dei til vanleg usynnelege checkpointa kan ein bruke bryteren oppe til venstre. Ein kan skru på/av lyd gjennom «sound»-knappen. Info vil bli vist ved å trykke på «info»-knappen. Det er også ein «auto crash» knapp som kan vere nyttig.

<img align="center" width=400 src="./resources/buttons.png">
<br><br>
I «shopen» kan ein kjøpe oppgraderingar om ein har råd. Om ikkje, så må ein ut på ein ny runde og prøve igjen!

<img align="center" width=400 src="./resources/shop.png">
