
import javax.swing.JComponent;
import javax.swing.JFrame;

import view.RacingView;
import car.CarPosition;
import car.CarVector;
import car.CarPhysics;
import controller.RacingController;
import director.RaceDirector;

public class Main {
    public static void main(String[] args) {

      CarPosition position = new CarPosition(0,0);
      CarVector speed = new CarVector(0, -2);
      RaceDirector masi = new RaceDirector(position);
      JComponent view = new RacingView(masi);
      CarPhysics phy = new CarPhysics(position, speed, 0.006, masi);
      new RacingController(view, phy, masi);

      JFrame frame = new JFrame("sem2"); 
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

      frame.setContentPane(view);

      frame.pack();
      frame.setVisible(true);

	}
}
