package car;

import director.MapTerrain;


/**
 * The car physics is based on this guide by 'Marco Monster' from 2003:
 * https://asawicki.info/Mirror/Car%20Physics%20for%20Games/Car%20Physics%20for%20Games.html
 * All the physics are based at vectors, and the CarVector class helps out the physics class with this.
 * 
 * The upgrading of the car is also implemented in this class using formulas like engine power
 * equals x + y*level, and the same concept with aerodynamics and grip.
 * 
 * The physics works good in a game type like this. Of course, they are not the most realistic, but they do the job.
 * The drifting is based on a 'if you drive faster than x, you will drift' principle. The way I implemented the drifting, it
 * is limited to understeering (the front tires sliding and therefore less turning than steering angle) which seems
 * to be working in a very consistent way, which is good for the game flow. The air and rolling resistance is probably the
 * most realistic part of the physics.
 */

public class CarPhysics {

    public final CarPosition position;
    public final CarVector vector;
    public final double steeringAngle;
    public final CarViewable masi;

    private double mass = 1000;
    private double engineForce = 0;

    private double dragCf = 0.47;

    private int turningL;
    private int turningR;
    private int accelerate;
    private int brake;

    private double drifting;

    private CarVector currentTraction = new CarVector(0, 0);
    private CarVector currentDrag = new CarVector(0, 0);
    private CarVector currentRollingResistance = new CarVector(0, 0);
    private CarVector totalLongForce = new CarVector(0, 0);
    private CarVector accel = new CarVector(0, 0);

    private MapTerrain surface;
    
    public CarPhysics(CarPosition position, CarVector vector, double steeringAngle, CarViewable masi) {
        this.position = position;
        this.vector = vector;
        this.steeringAngle = steeringAngle;
        this.masi = masi;
    }


    private void traction() {
        currentTraction.set(vector);
        currentTraction.getUnitVector();
        currentTraction.scaleVector(engineForce * surface.getEngineCf());
    }

    private void drag() {
        currentDrag.set(vector);
        currentDrag.scaleVector(-dragCf+masi.getLevel(2)*0.017);
        currentDrag.scaleVector(vector.getLength());
    }
    
    private void rolling() {
        currentRollingResistance.set(vector);
        currentRollingResistance.scaleVector(-30*(dragCf-masi.getLevel(2)*0.017));
    }

    private void totalLongForce() {
        totalLongForce.set(currentTraction);
        totalLongForce.deltaX(currentDrag.getX() + currentRollingResistance.getX());
        totalLongForce.deltaY(currentDrag.getY() + currentRollingResistance.getY());
    }

    private void accel() {
        accel.set(totalLongForce);
        accel.scaleVector(1/mass);
    }

    private void speed() {
        vector.deltaX(accel.getX());
        vector.deltaY(accel.getY());
        
        masi.setSpeed(vector.getLength());
    }

    private void position() {
        position.deltaX(vector.getX());
        position.deltaY(vector.getY());
    }

    private void slip() {

        double speed = vector.getLength();
        double wheelAngle = 0.4 * (32 +  masi.getLevel(3)*1.35);
        double slipNum = wheelAngle - speed;
        if (slipNum < 0) {
            drifting = 0.5;
            masi.drift(1);
        }
        else {
            drifting = 1;
            masi.drift(0);
        }
    }

    private void turningAngle(int i) {
        double turn = steeringAngle * vector.getLength() * drifting;
        vector.turn(i*turn);
    }


    public void accelerate(int accelerate) {
        this.accelerate = accelerate;
    }

    public void brake(int brake) {
        this.brake = brake;
    }

    public void turnLeft(int turning) {
        turningL = turning;
    }

    public void turnRight(int turning) {
        turningR = turning;
    }



    public void update() {

        this.surface = masi.getSurface();

        if (turningL == 1) {
            slip();
            turningAngle(-1);
        }
        else if (turningR == 1) {
            slip();
            turningAngle(1);
        }
        else {
            drifting = 1;
            masi.drift(0);
        }

        if (accelerate == 1) {
            engineForce = 292 + (masi.getLevel(1) * 8);
        }
        else if (brake == 1) {
            engineForce = -292 - (masi.getLevel(1) * 8);
        }
        else {
            engineForce = 0;
        }

        if (vector.sinDirection() > 0) {
            position.setCurrentAngle(Math.acos(vector.cosDirection()));
        }
        else if (vector.sinDirection() < 0) {
            position.setCurrentAngle(-Math.acos(vector.cosDirection()));
        }

        traction();
        drag();
        rolling();
        totalLongForce();
        accel();
        speed();
        position();
    }


    public void reset() {
        vector.reset();
    }
}
