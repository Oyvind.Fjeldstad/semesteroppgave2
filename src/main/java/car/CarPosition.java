package car;

/**
 * This class works keeps track of the position of the car and the angle of rotation.
 */

public class CarPosition {

    private double xValue;
    private double yValue;

    private double currentAngle;
    
    public CarPosition(int startX, int startY) {
        this.xValue = startX;
        this.yValue = startY;
    }

    public double getX() {
        return xValue;
    }

    public double getY() {
        return yValue;
    }

    public void deltaX(double x) {
        this.xValue += x;
    }

    public void deltaY(double y) {
        this.yValue += y;
    }

    public void setPos (double x, double y) {
        this.xValue = (int) x;
        this.yValue = (int) y;
    }

    public double getCurrentAngle() {
        return currentAngle;
    }

    public void setCurrentAngle(double currentAngle) {
        this.currentAngle = currentAngle;
    }

    public void reset() {
        this.xValue = 0;
        this.yValue = 0;
    }
}
