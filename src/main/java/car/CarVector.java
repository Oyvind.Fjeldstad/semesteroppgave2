package car;

/**
 * This class both works as a template for two-dimensional vectors and to
 * keep track of the speed. Most forces that works on the car is represented 
 * by a CarVector, and the speed is the CarVector initialised in the Main method. 
 * 
 * The class also has the required methods to do the maths required in the CarPhysics class. 
 */

public class CarVector {

    public double x;
    public double y;

    public CarVector(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void set(CarVector vector) {
        this.x = vector.getX();
        this.y = vector.getY();
    }


    public double getX() {
        return x;
    }
    
    public double getY() {
        return y;
    }

    public void scaleVector(double scalar) {
        this.x *= scalar;
        this.y *= scalar;
    }

    public void getUnitVector() {
        double getLength = getLength();
        this.x /= getLength;
        this.y /= getLength;
    }

    public void deltaX(double deltaX) {
        this.x += deltaX;
    }

    public void deltaY(double deltaY) {
        this.y += deltaY;
    }

    public double getLength() {
        return Math.sqrt(x*x + y*y);
    }

    public double cosDirection() {
        return x / getLength();
    }

    public double sinDirection() {
        return y / getLength();
    }

    public void turn(double angle) {
        
        double newX = this.x * Math.cos(angle) - this.y * Math.sin(angle);
        double newY = this.x * Math.sin(angle) + this.y * Math.cos(angle);

        this.x = newX;
        this.y = newY;
    }

    public void reset() {
        this.x = 0;
        this.y = -2;
    }
}