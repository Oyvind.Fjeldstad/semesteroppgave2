package car;

import director.MapTerrain;

public interface CarViewable {
    
    /**
     * @param select the car performance attribute, 1 for engine, 2 for aero and 3 for grip
     * @return the level of the selected car performance attribute
     */
    public int getLevel(int select);

    /**
     * @return the current surface the car drives at
     */
    public MapTerrain getSurface();

    /**
     * Informs the RaceDirector if the car is drifting or not
     * @param doesDrift 0 if not drifting, 1 if the car drifts
     */
    public void drift(int doesDrift);

    /**
     * Informs the RaceDirector what the speed is
     * @param speed the speed as a double
     */
    public void setSpeed(double speed);

}
