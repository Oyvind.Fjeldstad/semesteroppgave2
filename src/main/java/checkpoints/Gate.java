package checkpoints;

/**
 * This class has all checkpoints saved as a "Gate". It also has lists
 * with the gates the different levels has to pass through.
 */
class Gate {

    private final int xPos;
    private final int yPos;
    private final int width;
    private final int height;
    
    private Gate(int xPos, int yPos, int width, int height) {
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = width;
        this.height = height;
    }


    protected boolean isWithin(int x, int y) {
        if (x >= xPos & x <= xPos + width) {
            if (y >= yPos & y <= yPos + height) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }



    protected int getXPos() {
        return xPos;
    }

    protected int getYPos() {
        return yPos;
    }

    protected int getWidth() {
        return width;
    }

    protected int getHeight() {
        return height;
    }

    static final Gate tester = new Gate(100,0,200,400);

    // The gates for the first level.

    static final Gate A = new Gate(-153,-143,400,50);

    static final Gate B = new Gate(-143,-520,350,50);

    static final Gate C = new Gate(200,-760,50,240);

    static final Gate D = new Gate(263,-500,350,50);

    static final Gate E = new Gate(600,-335,50,350);

    static final Gate F = new Gate(880,-424,350,50);
    
    static final Gate G = new Gate(668,-700,350,50);

    static final Gate H = new Gate(980,-1000,50,300);

    static final Gate I = new Gate(1600,-930,50,290);

    static final Gate J = new Gate(1600,-600,50,300);

    static final Gate K = new Gate(1650,-280,50,270);

    static final Gate L = new Gate(1815,-315, 50, 290);

    static final Gate M = new Gate(1850,-25,50,360);

    static final Gate N = new Gate(1290,204,50,340);

    static final Gate O = new Gate(660,456,50,300);

    static final Gate P = new Gate(380,230,300,50);

    static final Gate Q = new Gate(360,40,50,200);



    // The gates for the second level.

    static final Gate A2 = new Gate(-85,-106,400,50);

    static final Gate B2 = new Gate(-80,-300,400,50);

    static final Gate C2 = new Gate(320,-640,50,340);

    static final Gate D2 = new Gate(357,-300,350,50);

    static final Gate E2 = new Gate(710,-270,50,350);

    static final Gate F2 = new Gate(1050,-630,50,260);

    static final Gate G2 = new Gate(1342,-370,50,250);

    static final Gate H2 = new Gate(1550,-630,50,220);

    static final Gate I2 = new Gate(1630,-430,340,50);

    static final Gate J2 = new Gate(1780,-130,400,50);

    static final Gate K2 = new Gate(1780,400,405,50);

    static final Gate L2 = new Gate(1770,400,50,355);
    
    static final Gate M2 = new Gate(1389,400,50,355);

    static final Gate N2 = new Gate(1080,100,50,330);

    static final Gate O2 = new Gate(860,360,50,385);

    static final Gate P2 = new Gate(650,100,50,330);

    static final Gate Q2 = new Gate(340,360,50,385);

    static final Gate R2 = new Gate(40,300,330,50);
    
    static final Gate S2 = new Gate(-85,100,400,50);


    // The gates for the third level.

    static final Gate A3 = new Gate(-62,-135,400,50);

    static final Gate B3 = new Gate(-62,-710,400,50);

    static final Gate C3 = new Gate(324,-1030,50,300);

    static final Gate D3 = new Gate(760,-680,50,350);

    static final Gate E3 = new Gate(1100,-1000,50,350);

    static final Gate F3 = new Gate(2100,-1000,50,350);
    
    static final Gate G3 = new Gate(2080,-700,50,300);

    static final Gate H3 = new Gate(2095,-400,50,300);

    static final Gate I3 = new Gate(3360,-750,50,350);

    static final Gate J3 = new Gate(3700,-400,300,50);

    static final Gate K3 = new Gate(3400,-450,50,370);

    static final Gate L3 = new Gate(2675,-220,50,350);

    static final Gate M3 = new Gate(2380,130,50,300);

    static final Gate N3 = new Gate(2100,544,300,50);

    static final Gate O3 = new Gate(2380,700,50,300);
    
    static final Gate P3 = new Gate(2410,1000,300,50);

    static final Gate Q3 = new Gate(2860,1000,50,300);

    static final Gate R3 = new Gate(3340,700,50,300);
    
    static final Gate S3 = new Gate(4185,430,50,350);

    static final Gate T3 = new Gate(4450,800,300,50);

    static final Gate U3 = new Gate(4108,800,50,300);

    static final Gate V3 = new Gate(3700,1322,50,350);

    static final Gate W3 = new Gate(3222,1455,50,350);
    
    static final Gate X3 = new Gate(2700,1730,300,50);

    static final Gate Y3 = new Gate(3140,1800,50,350);

    static final Gate Z3 = new Gate(3530,1700,50,350);
    
    static final Gate AA3 = new Gate(3515,2012,50,350);

    static final Gate BB3 = new Gate(1444,2116,50,350);
    
    static final Gate CC3 = new Gate(1030,2080,400,50);

    static final Gate DD3 = new Gate(1380,1750,500,50);

    static final Gate EE3 = new Gate(1030,1400,500,50);

    static final Gate FF3 = new Gate(1380,1100,500,50);
    
    static final Gate GG3 = new Gate(1030,800,500,50);

    static final Gate HH3 = new Gate(1380,500,509,50);
    
    static final Gate II3 = new Gate(1030,200,500,50);

    static final Gate JJ3 = new Gate(1380,-50,500,50);
    
    static final Gate KK3 = new Gate(1187,-270,50,250);

    static final Gate LL3 = new Gate(780,-50,380,50);
    
    static final Gate MM3 = new Gate(350,620,450,50);

    static final Gate NN3 = new Gate(330,668,50,350);

    static final Gate OO3 = new Gate(-67,600,400,50);
    


    static final Gate[] gates1 = {A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, A};

    static final Gate[] gates2 = {A2, B2, C2, D2, E2, F2, G2, H2, I2, J2, K2, L2, M2, N2, O2, P2, Q2, R2, S2, 
        A2, B2, C2, D2, E2, F2, G2, H2, I2, J2, K2, L2, M2, N2, O2, P2, Q2, R2, S2, A2};

    static final Gate[] gates3 = {A3, B3, C3, D3, E3, F3, G3, H3, I3, J3, K3, L3, M3, N3, O3, P3, Q3, R3, S3, T3, U3, V3, W3, X3, Y3, Z3, 
        AA3, BB3, CC3, DD3, EE3, FF3, GG3, HH3, II3, JJ3, KK3, LL3, MM3, NN3, OO3, A3};
}
