package checkpoints;

import java.awt.Graphics;
import java.awt.Color;


/**
 * This class communicates with the RaceDirector and do all the work in regards to the gates.
 * Can be seen as a helper class for the RaceDirector.
 */
public class GateDirector {
    
    public GateDirector() {
    }

    public int getNumberofGates(int level) {
        if (level == 0) {  
            return Gate.gates1.length - 1;
        }
        else if (level == 1) {
            return Gate.gates2.length - 1;
        }
        else {
            return Gate.gates3.length - 1;
        }
    }

    public boolean checkGate(int x, int y, int num, int level) {
        Gate tester;
        if (level == 0) {        
            tester = Gate.gates1[num];
        }
        else if (level == 1) {
            tester = Gate.gates2[num];
        }
        else {            
            tester = Gate.gates3[num];
        }
        return tester.isWithin(x, y);
    }

    public void drawGates(Graphics canvas, int deltaX, int deltaY, int level) {
        canvas.setColor(new Color(0,183,235,190));

        Gate[] current;
        if (level == 0) {        
            current = Gate.gates1;
        }
        else if (level == 1) {
            current = Gate.gates2;
        }
        else {            
            current = Gate.gates3;
        }

        for (int i = 0; i < getNumberofGates(level) + 1; i++) {
            Gate thisGate = current[i];
            canvas.fillRect(thisGate.getXPos()-deltaX, thisGate.getYPos()-deltaY, thisGate.getWidth(), thisGate.getHeight());
        }
    }

    public void drawGate(Graphics canvas, int num, int deltaX, int deltaY, int level) {
        canvas.setColor(new Color(252,15,192,190));

        Gate[] current;
        if (level == 0) {        
            current = Gate.gates1;
        }
        else if (level == 1) {
            current = Gate.gates2;
        }
        else {            
            current = Gate.gates3;
        }

        Gate thisGate = current[num];
        canvas.fillRect(thisGate.getXPos()-deltaX, thisGate.getYPos()-deltaY, thisGate.getWidth(), thisGate.getHeight());
    }
}