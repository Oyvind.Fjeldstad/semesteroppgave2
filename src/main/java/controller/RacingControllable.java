package controller;

import director.GameState;

public interface RacingControllable {
    
    /**
     * The game state is the general state of the game.
     * @return the current game state
     */
    public GameState getGameState();

    /**
     * The screen state is a spesific state of the game underneath the game state.
     * @return the current screen state.
     */
    public int getScreenState();
    
    /**
     * Sets the screen state, spesific state of the game underneath the game state.
     * @param i the screen state to be applied.
     */
    public void setScreenState(int i);

    /**
     * Sets the name in the race director.
     * @param name the name to be applied
     * @param select 0 for player name, 1 for car name
     */
    public void setName(String name, int select);

    /**
     * Updates the race director.
     */
    public void update();

    /**
     * Resets the race director.
     */
    public void reset();

    /**
     * Sets the game state to active.
     */
    public void setActive();

    /**
     * Buys a tick of the selected attribute, if one can afford.
     * @param select the attribute you want to upgrade. 1 for engine, 2 for aero, 3 for grip.
     */
    public void buy(int select);

    /**
     * Sets the checkpoint status.
     * @param i 1 if visible, 0 if not visible.
     */
    public void setCheckpointStatus(int i);
    
    /**
     * @return the checkpoint status, 1 if visible, 0 if not visible
     */
    public int getCheckpointStatus();

    /**
     * Restarts the RaceDirector. The game will be restarted to the state 
     * at the start.
     */
    public void restart();

    /**
     * Tells the RaceDirector that the car has crashed.
     */
    public void crash();

    /**
     * Sets if sounds whould be on or off.
     * @param setting 0 if off, 1 if on
     */
    public void setSound(int setting);

    /**
     * @return the sound status, 1 if on, 0 if off
     */
    public int getSound();
    
}
