package controller;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.Timer;

import java.awt.event.MouseEvent;
import java.awt.MouseInfo;
import view.Button;

import javax.swing.JComponent;
import car.CarPhysics;
import director.GameState;

/**
 * The control centre of the game. It takes keystrokes, mouse position and mouseclicks.
 * The game runs at 50hz (0,02s/50 times per second) for 'game-ticks'. 
 */

public class RacingController implements java.awt.event.KeyListener, java.awt.event.ActionListener, java.awt.event.MouseListener{

    private final CarPhysics phy;
    private final JComponent racingView;
    private final RacingControllable masi;
    private Timer timer;
    private String name = "";
    private String carName = "";
    private int dummy = -1;
    
    public RacingController(JComponent racingView, CarPhysics phy, RacingControllable masi) {
        this.phy = phy;
        this.racingView = racingView;
        this.masi = masi;
        racingView.addKeyListener(this);
        racingView.addMouseListener(this);
        this.timer = new Timer(20, this);
        timer.start();
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // TODO Auto-generated method stub
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (masi.getGameState() == GameState.ACTIVE_GAME) {
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                phy.turnLeft(1);
                racingView.repaint();
            }
    
            else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                phy.turnRight(1);
                racingView.repaint();
            }
    
            else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                phy.brake(1);
                racingView.repaint();
            }
    
            else if (e.getKeyCode() == KeyEvent.VK_UP) {
                phy.accelerate(1);
                racingView.repaint();
            }
            else if (masi.getScreenState() == 141) {
                masi.setScreenState(140);
            }
        }

        else if (masi.getGameState() == GameState.START_SCREEN) {

            if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
            }
            else if (e.getKeyCode() == KeyEvent.VK_CONTROL) {
            }
            else if (e.getKeyCode() == KeyEvent.VK_ALT) {
            }
            else if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
                if (name.length() > 0 & masi.getScreenState() == 0) {
                    name = name.substring(0,name.length()-1);
                    masi.setName(name, 0);
                }
                else if (carName.length() > 0 & masi.getScreenState() == 1) {
                    carName = carName.substring(0,carName.length()-1);
                    masi.setName(carName, 1);
                }
            }   
            else {
                if (masi.getScreenState() == 0) {
                    if (name.length() < 12) {
                        name += e.getKeyChar();
                        masi.setName(name, 0);
                    }
                }   
                else if (masi.getScreenState() == 1) {
                    if (carName.length() < 12) {
                        carName += e.getKeyChar();
                        masi.setName(carName, 1);
                    }
                }
            }
            racingView.repaint();
        }
        else if (masi.getGameState() == GameState.SHOP) {
            if (e.getKeyCode() == KeyEvent.VK_R) {
                masi.reset();
                phy.reset();
                masi.setActive();
                racingView.repaint();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            phy.turnLeft(0);
        }
        else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {  
            phy.turnRight(0); 
        }
        else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            phy.brake(0);
        }

        else if (e.getKeyCode() == KeyEvent.VK_UP) {
            phy.accelerate(0);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int x = (int) MouseInfo.getPointerInfo().getLocation().getX();
        int y = (int) MouseInfo.getPointerInfo().getLocation().getY() - 30;

        if (masi.getGameState() == GameState.ACTIVE_GAME) {
            phy.update();
            masi.update();
            racingView.repaint();

            Button checkpoints = Button.buttons[10];
            if (checkpoints.isWithin(x, y)) {
                masi.setScreenState(100);
            }
            else if (masi.getScreenState() == 100) {
                masi.setScreenState(10);
            }
            Button autocrash = Button.buttons[13];
            if (autocrash.isWithin(x, y)) {
                masi.setScreenState(130);
            }
            else if (masi.getScreenState() == 130) {
                masi.setScreenState(10);
            }
            Button info = Button.buttons[14];
            if (info.isWithin(x, y)) {
                if (masi.getScreenState() != 141) {
                    masi.setScreenState(140);
                }
            }
            else if (masi.getScreenState() == 140) {
                    masi.setScreenState(10);
            }
            Button sound = Button.buttons[15];
            if (sound.isWithin(x, y)) {
                masi.setScreenState(150);
            }
            else if (masi.getScreenState() == 150) {
                masi.setScreenState(10);
            }
        }
        else if (masi.getGameState() == GameState.START_SCREEN) {
            Button nameField = Button.buttons[0];
            if (masi.getScreenState() != 0) {
                if (nameField.isWithin(x, y)) {
                    masi.setScreenState(2);
                }
                else if (masi.getScreenState() == 2) {
                    masi.setScreenState(1);
                }
            }
            Button carNameField = Button.buttons[1];
            if (masi.getScreenState() != 1) {
                if (carNameField.isWithin(x, y)) {
                    masi.setScreenState(3);
                }
                else if (masi.getScreenState() == 3) {
                    masi.setScreenState(0);
                }
            }
            Button start = Button.buttons[2];
            if (start.isWithin(x, y)) {
                if (masi.getScreenState() != 4) {
                    dummy = masi.getScreenState();
                }
                masi.setScreenState(4);
            }
            else if (masi.getScreenState() == 4) {
                masi.setScreenState(dummy);
            }
        }
        else if (masi.getGameState() == GameState.SHOP) {
            Button start = Button.buttons[2];
            if (start.isWithin(x, y)) {
                masi.setScreenState(20);
            }
            else if (masi.getScreenState() == 20) {
                masi.setScreenState(10);
            }
            Button engineUpgrade = Button.buttons[7];
            if (engineUpgrade.isWithin(x, y)) {
                masi.setScreenState(7);
            }
            else if (masi.getScreenState() == 7) {
                masi.setScreenState(10);
            }
            Button aeroUpgrade = Button.buttons[8];
            if (aeroUpgrade.isWithin(x, y)) {
                masi.setScreenState(8);
            }
            else if (masi.getScreenState() == 8) {
                masi.setScreenState(10);
            }
            Button gripUpgrade = Button.buttons[9];
            if (gripUpgrade.isWithin(x, y)) {
                masi.setScreenState(9);
            }
            else if (masi.getScreenState() == 9) {
                masi.setScreenState(10);
            }
        }
        else if (masi.getGameState() == GameState.GAME_OVER) {
            Button restart = Button.buttons[11];
            if (restart.isWithin(x, y)) {
                masi.setScreenState(-11);
            }
            else if (masi.getScreenState() == -11) {
                masi.setScreenState(-10);
            }
            Button quit = Button.buttons[12];
            if (quit.isWithin(x, y)) {
                masi.setScreenState(-12);
            }
            else if (masi.getScreenState() == -12) {
                masi.setScreenState(-10);
            }

        }
        racingView.repaint();
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        
        int x = me.getX();
        int y = me.getY();

        if (masi.getGameState() == GameState.START_SCREEN) {
            Button nameField = Button.buttons[0];
            if (nameField.isWithin(x, y)) {
                masi.setScreenState(0);
            }
            Button carNameField = Button.buttons[1];
            if (carNameField.isWithin(x, y)) {
                masi.setScreenState(1);
            }

            Button start = Button.buttons[2];
            if (start.isWithin(x, y)) {
                masi.setActive();
            }
        }   
        else if (masi.getGameState() == GameState.SHOP) {
            Button start = Button.buttons[2];
            if (start.isWithin(x, y)) {
                masi.reset();
                phy.reset();
                masi.setActive();
                racingView.repaint();
            }
            Button engineUpgrade = Button.buttons[7];
            if (engineUpgrade.isWithin(x, y)) {
                masi.buy(1);
            }
            Button aeroUpgrade = Button.buttons[8];
            if (aeroUpgrade.isWithin(x, y)) {
                masi.buy(2);
            }
            Button gripUpgrade = Button.buttons[9];
            if (gripUpgrade.isWithin(x, y)) {
                masi.buy(3);
            }
        }
        else if (masi.getGameState() == GameState.ACTIVE_GAME) {
            Button checkpoints = Button.buttons[10];
            if (checkpoints.isWithin(x, y)) {
                if (masi.getCheckpointStatus() == 1) {    
                    masi.setCheckpointStatus(0);
                }
                else  {
                    masi.setCheckpointStatus(1);
                }
            }
            Button autocrash = Button.buttons[13];
            if (autocrash.isWithin(x, y)) {
                masi.crash();
            }
            Button info = Button.buttons[14];
            if (info.isWithin(x, y)) {
                if (masi.getScreenState() == 141) {
                    masi.setScreenState(10);
                }
                else {
                    masi.setScreenState(141);
                }
            }
            Button sound = Button.buttons[15];
            if (sound.isWithin(x, y)) {
                if (masi.getSound() == 0) {
                    masi.setSound(1);
                }
                else {
                    masi.setSound(0);
                }
            }
        }
        else if (masi.getGameState() == GameState.GAME_OVER) {
            Button restart = Button.buttons[11];
            if (restart.isWithin(x, y)) {
                phy.reset();
                masi.restart();
            }
            Button quit = Button.buttons[12];
            if (quit.isWithin(x, y)) {
                System.exit(0);
            }
        }
        racingView.repaint();
    }

    @Override
    public void mouseEntered(MouseEvent arg0) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mouseExited(MouseEvent arg0) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mousePressed(MouseEvent arg0) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mouseReleased(MouseEvent arg0) {
        // TODO Auto-generated method stub
        
    }
}

