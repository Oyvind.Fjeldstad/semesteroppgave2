package director;

/**
 * The different genereal game states.
 * Represented this as an enum to make the code easy to work with and understand.
 */
public enum GameState {
	START_SCREEN,
	ACTIVE_GAME,
    SHOP,
	GAME_OVER;

	private GameState() {
	}
}