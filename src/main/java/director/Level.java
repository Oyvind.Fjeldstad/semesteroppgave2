package director;

/**
 * The different levels in the game is represented by this class.
 * They all have a target time which one has to be faster than to
 * move on to the naxt level, a name, and the target time as a string.
 * 
 * To adjust difficulty, simply change the target and the corresponding lapTime-String.
 */

class Level {

    private final double target;
    private final String name;
    private final String lapTime;
    
    private Level(double target, String name, String lapTime) {
        this.target = target;
        this.name = name;
        this.lapTime = lapTime;
    }


    protected double getTarget() {
        return target;
    }

    protected String getName() {
        return name;
    }

    protected String getLapTime() {
        return lapTime;
    }


    static final Level A = new Level(8.5, "Renault Clio Cup", "00:08.50");

    static final Level B = new Level(13, "GP2", "00:13.00");

    static final Level C = new Level(22, "F1", "00:22.00");

    static final Level D = new Level(0, "WELTMEISTER", "0");

    static final Level[] levels = {A, B, C, D};
}
