package director;

/**
 * The different sorts of terrain at the map is represented by this class.
 * One can easily make this class more advanced by implementing for example
 * grip coeffients and more. Now it contains a engine coeffient which in the
 * physics is implemented so that the power get scaled. Some surfaces like grass
 * and gravel provide less grip and therefor less engine power. The terrain is 
 * reckognized through color.
 */
public class MapTerrain {

    private final int color;
	private final double engineCf;


    private MapTerrain(int color, double engineCf) {
        this.color = color;
		this.engineCf = engineCf;
    }

    protected int getColor() {
        return this.color;
    }

    public double getEngineCf() {
		return this.engineCf;
	}
	
	static final MapTerrain TRACK = new MapTerrain(-8421505,1);

	static final MapTerrain EDGE = new MapTerrain(-16777216,0.9);

	static final MapTerrain KERB1 = new MapTerrain(-1,0.8);

	static final MapTerrain KERB2 = new MapTerrain(-1237980,0.8);

	static final MapTerrain KERB3 = new MapTerrain(-12629812,0.8);

	static final MapTerrain YELLOW_TRAMPOLINE = new MapTerrain(-3584,-3);

	static final MapTerrain GRASS = new MapTerrain(-14503604,0.5);

	static final MapTerrain GRAVEL = new MapTerrain(-1055568,0.5);

	static final MapTerrain OB = new MapTerrain(-20791,0);

    static final MapTerrain[] surfaces = {TRACK,EDGE,KERB1,KERB2,KERB3,YELLOW_TRAMPOLINE,GRASS,GRAVEL,OB};
}