package director;

import checkpoints.GateDirector;
import view.RacingViewable;
import car.CarPosition;
import car.CarViewable;

import java.awt.Graphics;
import controller.RacingControllable;

/**
 * The class that controls most of what is happening in the game.
 * The RaceDirector is in other classes reffered to as 'masi', as an honour
 * to the great Michael Masi.
 * 
 * The main function of this class is to hold and exchange information about the 
 * state of the game between classes. For example tell the view what the current
 * car position is. Or tell the controller if we are in 'ACTIVE_GAME' or in the 'SHOP'.
 * 
 * Through it's helping classes it also controls the shop, sounds, terrain and levels.
 * 
 * Because of it's nature it has a lot of set/get methods, but also some more complex methods
 * which for example controls which checkpoint is the next and if a lap is completed correctly.
 */

public class RaceDirector implements RacingViewable, RacingControllable, CarViewable {

    private final CarPosition pos;
    private final GateDirector man;
    private final Shop shop;
    private final Sounds soundPlayer;

    private int numberofGates;

    private GameState state;

    private int activeGate = 0;
    private double lapTime = 0;
    private String name = "";
    private String carName = "";
    private int screenState = 0;
    private String lapTimeString = "";
    private int lastIncome;

    private int intLevel = 0;
    private Level currentLevel = Level.levels[intLevel];

    private String target = currentLevel.getLapTime();
    private double targetLap = currentLevel.getTarget();
    private String nextSeries = Level.levels[intLevel+1].getName();
    private String currentSeries = currentLevel.getName();

    private String levelUpMsg = "";

    private MapTerrain currentSurface = MapTerrain.TRACK;
    private int doesDrift = 0;
    private int showCheckpoints = 0;
    private double speed = 0;
    private double lastCheckpoint;
    private int sound = 1;

    public RaceDirector(CarPosition pos) {
        this.pos = pos;
        this.man = new GateDirector();
        this.shop = new Shop();
        this.soundPlayer = new Sounds();
        this.numberofGates = man.getNumberofGates(intLevel);
        this.state = GameState.START_SCREEN;
    }

    public void update() {

        if (man.checkGate((int) pos.getX(), (int) pos.getY(), activeGate, intLevel) == true) {
            if (activeGate == numberofGates) {
                int min = (int) lapTime / 60;
                int sec = (int) lapTime - min * 60;
                double decimal = Math.round((lapTime - sec - (min*60))*100);
                if (decimal == 100) {
                    decimal = 0;
                    sec += 1;
                }
                String decimalOutput = String.format("%02d", (int) decimal);
                if (min != 0) {  
                    lapTimeString = min + ":" + sec + "." + decimalOutput + " min";
                }
                else {
                    lapTimeString = sec + "." + decimalOutput + " sec";
                }
                
                if (lapTime < targetLap) {
                    if (currentSeries == "F1") {
                        state = GameState.GAME_OVER;
                        screenState = -10;
                        soundPlayer.playWeltmeister();
                        return;
                    }
                    intLevel += 1;
                    this.numberofGates = man.getNumberofGates(intLevel);

                    currentLevel = Level.levels[intLevel];
                    target = currentLevel.getLapTime();
                    targetLap = currentLevel.getTarget();
                    nextSeries = Level.levels[intLevel+1].getName();
                    currentSeries = currentLevel.getName();
                    levelUpMsg = "Gralla, level UP!";
                }
                else {
                    levelUpMsg = "";
                }

                if (2000*(intLevel+1) -lapTime*100 > 0) {
                    income((int)(2000*(intLevel+1) - lapTime * 100));
                }
                else {
                    income(0);
                }


                this.state = GameState.SHOP;
                activeGate = 0;
                lapTime = 0;
                screenState = 10;
                lastCheckpoint = 0;
            }
            else { 
                activeGate+=1;
                lapTime += 0.02;
                lastCheckpoint = lapTime;
            }
        }
        else {
            /* To have an engine sound, remove the comment.
            The engine sound is fairly loud...

            if ((Math.round(lapTime*10000)/10000.0)%2 == 0) {
                if (sound == 1) {
                    soundPlayer.playEngine();
                }
            }
            */
            lapTime += 0.02;
        }
    }

    public void drawGates(Graphics canvas) {
        if (showCheckpoints == 1) {
            man.drawGates(canvas, (int)pos.getX() - 550, (int)pos.getY() - 375, intLevel);
            man.drawGate(canvas, activeGate, (int)pos.getX() - 550, (int)pos.getY() - 375, intLevel);
        }
    }

    public void setCurrentTerrain(int col) {
        for (int i = 0; i < MapTerrain.surfaces.length; i++) {
            if (col == MapTerrain.surfaces[i].getColor()) {
                if (MapTerrain.surfaces[i] == MapTerrain.OB) {
                    crash();
                }
                currentSurface = MapTerrain.surfaces[i];
            }
        }
    }

    public void crash() {
        state = GameState.SHOP;
        activeGate = 0;
        lapTime = 0;
        screenState = 10;
        lapTimeString = "CRASH";
        levelUpMsg = "";
        income(0);
        if (sound == 1) {
            soundPlayer.playCrash();
        }
    }


    public GameState getGameState() {
        return state;
    }

    public void setActive() {
        this.state = GameState.ACTIVE_GAME;
    }

    public void reset() {
        pos.reset();
    }

    public void setName(String name, int i) {
        if (i == 0) {
            this.name = name;
        }
        else {
            this.carName = name;
        }
    }

    public String getName(int i) {
        if (i == 0) {
            return name;
        }
        else {
            return carName;
        }
    }   

    public void setScreenState(int i) {
        this.screenState = i;
    }

    public int getScreenState() {
        return screenState;
    }

    public String getLapTime(int i) {
        if (i == 0) {
            return lapTimeString;
        }
        else {
            int min = (int) lapTime / 60;
            int sec = (int) lapTime - min * 60;
            double decimal = Math.round((lapTime - sec - (min*60))*100);
            if (decimal == 100) {
                decimal = 0;
                sec += 1;
            }
            String decimalOutput = String.format("%02d", (int) decimal);
            if (min != 0) {  
                return min + ":" + sec + "." + decimalOutput;
            }
            else {
                return  sec + "." + decimalOutput;
            }
        }
    }

    public int getLevel(int i) {
        if (i == 1) {
            return shop.getEngineLevel();
        }
        else if (i == 2) {
            return shop.getAeroLevel(); 
        }
        else if (i == 3) {
            return shop.getGripLevel();
        }
        else {
            return 0;
        }
    }

    public int getPrice(int i) {
        if (i == 1) {
            return shop.getEnginePrice();
        }
        else if (i == 2) {
            return shop.getAeroPrice(); 
        }
        else if (i == 3) {
            return shop.getGripPrice();
        }
        else {
            return 0;
        }
    }

    public int getBal() {
        return shop.getBalance();
    }

    public void income(int income) {
        lastIncome = income;
        shop.income(income);
    }

    public int getLastIncome() {
        return lastIncome;
    }

    public void buy(int selected) {
        shop.buy(getPrice(selected), selected);
    }

    public String getTargetLapTime() {
        return target;
    }

    public String getNextSeries() {
        return nextSeries;
    }

    public String getCurrentSeries() {
        return currentSeries;
    }

    public String getLevelUpMsg() {
        return levelUpMsg;
    }

    public int getCurrentLevel() {
        return intLevel;
    }


    public MapTerrain getSurface() {
        return currentSurface;
    }


    public int grassOrGravel() {
        if (currentSurface == MapTerrain.GRASS || currentSurface == MapTerrain.GRAVEL) {
            if (currentSurface == MapTerrain.GRASS & intLevel == 1) {
                return 0;
            }
            return 1;
        }
        else {
            return 0;
        }
    }

    public void drift(int doesDrift) {
        this.doesDrift = doesDrift;
    }

    public int doDrift() {
        return doesDrift;
    }

    public void setCheckpointStatus(int i) {
        showCheckpoints = i;
    }

    public int getCheckpointStatus() {
        return showCheckpoints;
    }


    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public int getSpeed() {
        return (int) (speed*5);
    }

    public String getDelta() {
        double partTime = targetLap/(numberofGates+1);
        double delta = lastCheckpoint - partTime*activeGate;
        int min = (int) delta / 60;
        int sec = (int) delta - min * 60;
        double decimal = Math.round((delta - sec - (min*60))*100);
        if (decimal == 100) {
            decimal = 0;
            sec += 1;
        }
        String decimalOutputPluss = String.format("%02d", (int) decimal);
        String decimalOutputMinus = String.format("%02d", (int) (-decimal));
        if (delta > 0) {
            if (min != 0) {  
                return "+ " + min + ":" + sec + "." + decimalOutputPluss;
            }
            else {
                return "+ " +sec + "." + decimalOutputPluss;
            }
        }
        else {
            if (min != 0) {  
                return "- " + (-min) + ":" + (-sec) + "." + decimalOutputMinus;
            }
            else {
                return  "- " + (-sec) + "." + decimalOutputMinus;
            }
        }
    }

    public void restart() {
        reset();
        shop.restart();
        activeGate = 0;
        lapTime = 0;
        lastCheckpoint = 0;
        intLevel = 0;
        this.numberofGates = man.getNumberofGates(intLevel);
        currentLevel = Level.levels[intLevel];
        target = currentLevel.getLapTime();
        targetLap = currentLevel.getTarget();
        nextSeries = Level.levels[intLevel+1].getName();
        currentSeries = currentLevel.getName();
        setActive();
    }

    public int getSound() {
        return sound;
    }

    public void setSound(int setting){
        sound = setting;
    }

    @Override
    public double getX() {
        return pos.getX();
    }

    @Override
    public double getY() {
        return pos.getY();
    }

    @Override
    public double getCarAngle() {
        return pos.getCurrentAngle();
    }

}