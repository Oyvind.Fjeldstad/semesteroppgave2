package director;

/**
 * The 'bank' of the game. Controls all transactions, prices and levels of
 * the different performance attributes. A helper class to the Race Director.
 */
class Shop {

    private int balance = 0;

    private int engineLevel = 1;
    private int aeroLevel = 1;
    private int gripLevel = 1;

    private int enginePrice = 100;
    private int aeroPrice = 100;
    private int gripPrice = 100;

    Shop() {
    }
    
    protected void buy(int price, int selected) {

        if (price > balance) {
            return;
        }
        if (selected == 1) {
            if (engineLevel < 11) {
                engineLevel += 1;
            }
            else {
                return;
            }
        }
        if (selected == 2) {
            if (aeroLevel < 11) {
                aeroLevel += 1;
            }
            else {
                return;
            }
        }
        if (selected == 3) {
            if (gripLevel < 11) {
                gripLevel += 1;
            }
            else {
                return;
            }
        }

        balance -= price;
    }

    protected void income(int income) {
        balance += income;
    }

    protected int getBalance() {
        return balance;
    }

    protected int getEngineLevel() {
        return engineLevel;
    }
    protected int getAeroLevel() {
        return aeroLevel;
    }
    protected int getGripLevel() {
        return gripLevel;
    }

    protected int getEnginePrice() {
        if (engineLevel == 11) {
            return 0;
        }
        return enginePrice * engineLevel*engineLevel;
    }
    protected int getAeroPrice() {
        if (aeroLevel == 11) {
            return 0;
        }
        return aeroPrice * aeroLevel*aeroLevel;
    }
    protected int getGripPrice() {
        if (gripLevel == 11) {
            return 0;
        }
        return gripPrice * gripLevel*gripLevel;
    }

    protected void restart() {
        balance = 0;
        engineLevel = 1;
        aeroLevel = 1;
        gripLevel = 1;
    }
}
