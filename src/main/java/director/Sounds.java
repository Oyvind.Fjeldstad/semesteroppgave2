package director;

import java.io.File;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.util.Random;

/**
* A very simple wav-format audio player to have sound effects with good quality.
* I used a similar audioplayer in the first semester assignment.
*/
class Sounds {

    Sounds() {
    }
    
    /**
     * A method to play sounds of the wav-format.
     * @param filename the name of the file to play.
     */
    private void play(String filename) {
        try {
            Clip clip = AudioSystem.getClip();

            clip.open(AudioSystem.getAudioInputStream(new File(filename)));
            clip.start();
        }
        catch (Exception exc) {
            exc.printStackTrace(System.out);
        }
    }

    protected void playCrash() {
        Random ran = new Random();
        int x = ran.nextInt(2);
        
        play("./resources/crash" + x + ".wav");
    }

    protected void playWeltmeister() {
        play("./resources/weltmeister.wav");
    }

    protected void playEngine() {
        play("./resources/engine.wav");
    }
}
