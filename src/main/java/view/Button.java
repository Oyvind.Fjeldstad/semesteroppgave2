package view;


/**
 * A class much like the 'Gate' which has a setup for boxes.
 * Here there are buttons used to navigate inside the game.
 */
public class Button {

    private final int xPos;
    private final int yPos;
    private final int width;
    private final int height;
    
    private Button(int xPos, int yPos, int width, int height) {
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = width;
        this.height = height;
    }


    public boolean isWithin(int x, int y) {
        if (x >= xPos & x <= xPos + width) {
            if (y >= yPos & y <= yPos + height) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }


    public int getXPos() {
        return xPos;
    }

    public int getYPos() {
        return yPos;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    
    static final Button tester = new Button(100,0,200,400);

    static final Button nameField = new Button(50,300,500,100);
    static final Button carNameField = new Button(50,500,500,100);

    static final Button start = new Button(740,550,(int)(330*0.8),125);
    
    static final Button statBox = new Button(50,50,550,650);
    static final Button engine = new Button(650,150,400,100);
    static final Button aero = new Button(650,275,400,100);
    static final Button grip = new Button(650,400,400,100);
    static final Button upgradeEngine = new Button(910,155,135,90);
    static final Button upgradeAero = new Button(910,280,135,90);
    static final Button upgradeGrip = new Button(910,405,135,90);

    static final Button checkpoints = new Button(25,100,100,50);

    static final Button restart = new Button(300, 275, 200, 200);
    static final Button quit = new Button(600, 275, 200, 200);

    static final Button autocrash = new Button(25,200,100,50);

    static final Button info = new Button(975,100,100,50);
    static final Button sound = new Button(25,300,100,50);

    public static final Button[] buttons = {nameField, carNameField, start, statBox, engine, aero, grip, 
        upgradeEngine, upgradeAero, upgradeGrip, checkpoints, restart, quit, autocrash, info, sound};
    
}
