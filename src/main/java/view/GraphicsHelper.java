package view;

import java.awt.Graphics;
import java.awt.Color;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import java.util.ArrayList;

import java.awt.Font;

/**
 * A class that helps the RacingView.
 */

class GraphicsHelper {

    private final BufferedImage clio = getCar("clio");
    private final BufferedImage gp2 = getCar("gp2");
    private final BufferedImage f1 = getCar("f1");

    private final TireMark marks;

    GraphicsHelper() {
        marks = new TireMark();
    }

    private BufferedImage getCar(String type) {

        try {
            return ImageIO.read(new File("./resources/car" + type + ".png"));
            
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

    }

    protected void drawCar(Graphics canvas, double angle, double carX, double carY, int drawMarks, int level) {
        
        BufferedImage car = null;
        if (level == 0) {
            car = clio;
        }
        else if (level == 1) {
            car = gp2;
        }
        else if (level == 2){
            car = f1;
        }

        
        BufferedImage rotatedCar = new BufferedImage(1100, 750, car.getType());       
        Graphics2D g2 = rotatedCar.createGraphics();
        g2.rotate(angle + Math.PI/2, 550,375);
        g2.drawImage(car, null, 510, 285);

        AffineTransform transform = new AffineTransform();
        
        transform.translate(550, 375); 
        transform.rotate(angle);

        
        double deltaX = 40 * Math.sin(angle);
        double deltaY = 40 * Math.cos(angle);

        if (drawMarks == 1) {
            addMark(carX+deltaX+ Math.cos(angle)*60, carY-deltaY+ Math.sin(angle)*60);
            addMark(carX-deltaX+ Math.cos(angle)*60, carY+deltaY+ Math.sin(angle)*60);
        }

        drawMarks(canvas, carX, carY);        
        canvas.drawImage(rotatedCar, 0, 0, 1100, 750, null);
    }

    protected void drawButton(Graphics canvas, int num, Color clr) {
        canvas.setColor(clr);
        Button but = Button.buttons[num];
        canvas.fillRect(but.getXPos(), but.getYPos(), but.getWidth(), but.getHeight());
    }
    
    protected void drawButtonBorder(Graphics canvas, int num, Color clr) {
        canvas.setColor(clr);
        Button but = Button.buttons[num];
        canvas.drawRect(but.getXPos(), but.getYPos(), but.getWidth(), but.getHeight());
    }

    protected void drawStars(Graphics canvas, int num, int startX, int startY) {
        if (num != 11) {
            canvas.setColor(Color.YELLOW);
            for (int i = 0; i < num; i++) {
                canvas.fillRect(startX+i*30, startY, 25, 25);
            }
            canvas.setColor(Color.BLACK);
            for (int j = num; j < 10; j++) {
                canvas.fillRect(startX+j*30, startY, 25, 25);
            }
        }
        else {
            canvas.setColor(new Color(212,175,55));
            for (int i = 0; i < 10; i++) {
                canvas.fillRect(startX+i*30, startY, 25, 25);
            }
        }
    }

    protected void drawMarks(Graphics canvas, double currentX, double currentY) {
        ArrayList<TireMark> allMarks = marks.getTireMarks();
        for (int i = 0; i < allMarks.size(); i ++) {
            double x = allMarks.get(i).getPositionX() - currentX;
            double y = allMarks.get(i).getPositionY() - currentY;
            double width = allMarks.get(i).getWidth();
            double height = allMarks.get(i).getHeight();
            Color clr = allMarks.get(i).getColor();

            canvas.setColor(clr);
            canvas.fillRect((int)x, (int)y, (int)width, (int)height);
        }
    }

    protected void addMark(double x, double y) {
        marks.newTireMark(x, y);
    }

    protected void clearMarks() {
        marks.clearMarks();
    }

    protected void drawCheckpointSwitch(Graphics canvas, int status) {
        canvas.setColor(Color.BLACK);
        canvas.setFont(new Font("SansSerif", Font.BOLD, 20));
        canvas.drawString("Toggle", 25, 70);
        canvas.drawString("checkpoints", 25, 90);
        if(status == 1) {
            drawButton(canvas, 10, new Color(0,0,0,150));
            canvas.setColor(Color.GREEN);
            canvas.fillRect(25, 100, 50, 50);
        }
        if(status == 0) {
            drawButton(canvas, 10, new Color(0,0,0,150));
            canvas.setColor(Color.RED);
            canvas.fillRect(75, 100, 50, 50);
        }
    }

    protected void drawSpeedo(Graphics canvas, int speed) {
        canvas.setColor(new Color(0,0,0,150));
        canvas.fillRect(350, 650, 400, 100);
        canvas.setColor(Color.YELLOW);
        canvas.setFont(new Font("SansSerif", Font.BOLD, 50));
        canvas.drawString(String.valueOf(speed), 525, 725);
        canvas.setColor(Color.WHITE);
        canvas.setFont(new Font("SansSerif", Font.BOLD, 30));
        canvas.drawString("Speed:", 400, 720);
        canvas.drawString("px/ds", 630, 720);   
    }

    protected void drawTimeInfo(Graphics canvas, String time, String targetTime, String delta) {
        canvas.setColor(new Color(0,0,0,150));
        canvas.fillRect(350, 0, 400, 100);
        canvas.setColor(Color.YELLOW);
        canvas.setFont(new Font("SansSerif", Font.BOLD, 30));
        canvas.drawString(time, 520, 65);
        
        canvas.setColor(Color.WHITE);
        canvas.setFont(new Font("SansSerif", Font.BOLD, 20));
        canvas.drawString("Target: " + targetTime, 470, 30);  
        canvas.drawString("Delta: " + delta, 470, 90);  

        if (delta.charAt(0) == '-') {
            canvas.setColor(Color.GREEN);
            canvas.fillRect(630, 50, 50, 50);
        }
        else {
            canvas.setColor(Color.RED);
            canvas.fillRect(630, 50, 50, 50);
        }
    }

    protected void drawTrackMap(Graphics canvas, double carX, double carY, BufferedImage trackMap, 
        int relationship, int dividor, int startX, int startY) {
            
        canvas.setColor(new Color(0,0,0,50));
        canvas.fillRect(25, 525, trackMap.getWidth()/dividor, trackMap.getHeight()/dividor);
        canvas.drawImage(trackMap, 25, 525, trackMap.getWidth()/dividor, trackMap.getHeight()/dividor, null);
        canvas.setColor(Color.YELLOW);
        canvas.fillRect((int)carX/relationship+startX, (int)carY/relationship+startY, 10, 10);
    }

}
