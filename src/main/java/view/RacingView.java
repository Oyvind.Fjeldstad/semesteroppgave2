package view;

import javax.swing.JComponent;

import java.awt.Graphics;
import java.awt.Color;
import java.awt.Dimension;

import java.awt.Font;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import director.GameState;

/**
 * The class that paints the graphics.
 */
public class RacingView extends JComponent{

    public final RacingViewable masi;

    private final BufferedImage map1 = getMap(1);
    private final BufferedImage map2 = getMap(2);
    private final BufferedImage map3 = getMap(3);
    
    private final BufferedImage trackMap1 = getMap(11);
    private final BufferedImage trackMap2 = getMap(22);
    private final BufferedImage trackMap3 = getMap(33);

    private final BufferedImage pastor = loadPastor();
    private final GraphicsHelper helper;
    private Color brg = new Color(0,66,37);

    private int newRound = 0;
    private int rollingText = 0;

    {
        this.setFocusable(true);
    }

    public RacingView(RacingViewable masi) {
        this.masi = masi;
        this.helper = new GraphicsHelper();
    }    

    private BufferedImage getMap(int i) {

        try {
            return ImageIO.read(new File("./resources/oyvindromo" + i + ".png"));
            
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

    }

    private BufferedImage loadPastor() {

        try {
            return ImageIO.read(new File("./resources/pastor.png"));
            
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

    }
    
    @Override
    public void paint(Graphics canvas) {

        if (masi.getGameState() == GameState.ACTIVE_GAME) {
            active_game(canvas);
        }
        else if (masi.getGameState() == GameState.START_SCREEN) {
            start_screen(canvas);
        }
        else if (masi.getGameState() == GameState.SHOP) {
            shop_screen(canvas);
        }
        else if (masi.getGameState() == GameState.GAME_OVER) {
            game_over(canvas);
        }
    
    }

    private void active_game(Graphics canvas) {
        BufferedImage subImg = null;
        if (masi.getCurrentLevel() == 0) {
            subImg = map1.getSubimage((int) masi.getX()+900, (int) masi.getY()+1450, 1100, 750);
            canvas.drawImage(subImg, 0, 0, this.getWidth(), this.getHeight(), null);
            helper.drawTrackMap(canvas, masi.getX(), masi.getY(), trackMap1, 10, 5, 50, 633);
        }
        else if (masi.getCurrentLevel() == 1) {
            subImg = map2.getSubimage((int) masi.getX()+900, (int) masi.getY()+1000, 1100, 750);
            canvas.drawImage(subImg, 0, 0, this.getWidth(), this.getHeight(), null);
            helper.drawTrackMap(canvas, masi.getX(), masi.getY(), trackMap2, 10, 4, 40, 590);
        }
        else if (masi.getCurrentLevel() == 2){
            subImg = map3.getSubimage((int) masi.getX()+800, (int) masi.getY()+1650, 1100, 750);
            canvas.drawImage(subImg, 0, 0, this.getWidth(), this.getHeight(), null);
            helper.drawTrackMap(canvas, masi.getX(), masi.getY(), trackMap3, 20, 7, 30, 575);
        }
        
        int centerRGB = subImg.getRGB(550, 375);
        masi.setCurrentTerrain(centerRGB);
        if (newRound == 1) {
            helper.clearMarks();
            newRound = 0;
        }

        helper.drawCheckpointSwitch(canvas, masi.getCheckpointStatus());
        
        canvas.setColor(Color.RED);
        
        if (masi.grassOrGravel() == 1) {
            helper.drawCar(canvas, masi.getCarAngle(), masi.getX(), masi.getY(), 1, masi.getCurrentLevel());
        }
        else if (masi.doDrift() == 1) {
            helper.drawCar(canvas, masi.getCarAngle(), masi.getX(), masi.getY(), 1, masi.getCurrentLevel());
        }
        else {
            helper.drawCar(canvas, masi.getCarAngle(), masi.getX(), masi.getY(), 0, masi.getCurrentLevel());
        }

        masi.drawGates(canvas);
        helper.drawSpeedo(canvas, masi.getSpeed());
        helper.drawTimeInfo(canvas, masi.getLapTime(1), masi.getTargetLapTime(), masi.getDelta());
        helper.drawButton(canvas, 13, Color.MAGENTA);
        helper.drawButton(canvas, 14, Color.BLUE);
        if (masi.getSound() == 1) {
            helper.drawButton(canvas, 15, Color.GREEN);
        }
        else {
            helper.drawButton(canvas, 15, Color.RED);
        }
        canvas.setColor(Color.WHITE);
        canvas.setFont(new Font("SansSerif", Font.BOLD, 17));
        canvas.drawString("Auto-crash", 30, 230);
        canvas.drawString("INFO", 1005, 130);
        canvas.drawString("SOUND", 45, 330);
        if (masi.getScreenState() == 100) {
            helper.drawButtonBorder(canvas, 10, Color.WHITE);
        }
        else if (masi.getScreenState() == 130) {
            helper.drawButtonBorder(canvas, 13, Color.WHITE);
        }
        else if (masi.getScreenState() == 140) {
            helper.drawButtonBorder(canvas, 14, Color.WHITE);
        }
        else if (masi.getScreenState() == 150) {
            helper.drawButtonBorder(canvas, 15, Color.WHITE);
        }
        else if (masi.getScreenState() == 141) {
            helper.drawButton(canvas, 14, new Color(0,0,0,100));
            canvas.fillRect(775,200,300,500);
            canvas.setColor(Color.ORANGE);
            canvas.setFont(new Font("SansSerif", Font.BOLD, 20));
            canvas.drawString("Arrows to drive", 800, 250);
            
            canvas.drawString("Mouse to navigate menus", 800, 300);
            
            canvas.drawString("Drive fast", 800, 350);
            
            canvas.drawString("and it will all be alright!", 800, 400);

            canvas.drawString("Don't crash!", 800, 450);
            
            canvas.setFont(new Font("SansSerif", Font.BOLD, 15));
            canvas.drawString("Press anything except the arrows", 800, 550);
            canvas.drawString("to remove the info...", 800, 575);
        }
    }

    private void shop_screen(Graphics canvas) {
        canvas.setColor(brg);
        canvas.fillRect(0, 0, this.getWidth(), this.getHeight());
        helper.drawButton(canvas, 2, Color.BLUE);
        helper.drawButtonBorder(canvas, 3, Color.BLUE);
        helper.drawButtonBorder(canvas, 4, Color.BLUE);
        helper.drawButtonBorder(canvas, 5, Color.BLUE);
        helper.drawButtonBorder(canvas, 6, Color.BLUE);
        helper.drawButton(canvas, 7, Color.GREEN);
        helper.drawButton(canvas, 8, Color.GREEN);
        helper.drawButton(canvas, 9, Color.GREEN);
        
        canvas.setColor(Color.YELLOW);
        canvas.setFont(new Font("SansSerif", Font.BOLD, 24));
        canvas.drawString("Or press 'R' to go again", 740, 715);
        canvas.setFont(new Font("SansSerif", Font.BOLD, 45));
        canvas.drawString("START", 790, 625);
        
        canvas.setColor(Color.CYAN);
        canvas.drawString("Engine", 700, 218);
        canvas.drawString("Aero", 700, 343);        
        canvas.drawString("Grip", 700, 468);
        
        canvas.setFont(new Font("SansSerif", Font.BOLD, 70));
        canvas.drawString("Upgrades:", 670, 100);

        
        canvas.setColor(Color.BLACK);
        canvas.setFont(new Font("SansSerif", Font.BOLD, 30));

        if (masi.getPrice(1) == 0) {
            canvas.drawString("MAX", 940, 210);
        }
        else {  
            canvas.drawString(masi.getPrice(1) + "$", 925, 210);
        }
        if (masi.getPrice(2) == 0) {
            canvas.drawString("MAX", 940, 335);
        }
        else {  
            canvas.drawString(masi.getPrice(2) + "$", 925, 335);
        }
        if (masi.getPrice(3) == 0) {
            canvas.drawString("MAX", 940, 460);
        }
        else {  
            canvas.drawString(masi.getPrice(3) + "$", 925, 460);
        }
        
        canvas.setColor(Color.MAGENTA);
        canvas.setFont(new Font("SansSerif", Font.BOLD, 45));
        canvas.drawString("Car performance: ", 125, 475);
        canvas.setFont(new Font("SansSerif", Font.BOLD, 35));
        
        canvas.setColor(Color.CYAN);
        canvas.drawString("Last lap: " + masi.getLapTime(0), 100, 100);
        canvas.drawString("Earned ø-coins: " + masi.getLastIncome() + "$", 100, 150);
        canvas.setColor(Color.ORANGE);
        canvas.drawString("Current balance: " + masi.getBal() + "$", 100, 225);
        canvas.setColor(Color.YELLOW);
        canvas.drawString(masi.getLevelUpMsg(), 325, 300);
        canvas.setColor(Color.WHITE);
        canvas.drawString("Series: " + masi.getCurrentSeries(), 100, 300);
        canvas.drawString("Target lap: " + masi.getTargetLapTime(), 100, 350);
        canvas.drawString("Next level: " + masi.getNextSeries(), 100, 400);

        canvas.setColor(Color.MAGENTA);
        canvas.drawString("Engine: ", 100, 550);
        canvas.drawString("Aero: ", 100, 600);
        canvas.drawString("Grip: ", 100, 650);
        helper.drawStars(canvas, masi.getLevel(1), 250, 528);
        helper.drawStars(canvas, masi.getLevel(2), 250, 578);
        helper.drawStars(canvas, masi.getLevel(3), 250, 628);

        if (masi.getScreenState() == 20) {
            helper.drawButtonBorder(canvas, 2, Color.YELLOW);
        }
        else if (masi.getScreenState() == 7) {
            helper.drawButton(canvas, 7, new Color(0,0,0,50));
        }
        else if (masi.getScreenState() == 8) {
            helper.drawButton(canvas, 8, new Color(0,0,0,50));
        }
        else if (masi.getScreenState() == 9) {
            helper.drawButton(canvas, 9, new Color(0,0,0,50));
        }

        newRound = 1;
    }

    private void start_screen(Graphics canvas) {
        canvas.setColor(brg);
        canvas.fillRect(0, 0, this.getWidth(), this.getHeight());
        canvas.drawImage(pastor, 740, 50, (int)(330*0.8), (int)(557*0.8), null);
        helper.drawButton(canvas, 2, Color.BLUE);

        if (masi.getScreenState() == 0) {
            helper.drawButton(canvas, 0, new Color(0,0,0,200));
            helper.drawButton(canvas, 1, new Color(0,0,0,25));
        }
        else if (masi.getScreenState() == 1) {
            helper.drawButton(canvas, 0, new Color(0,0,0,25));
            helper.drawButton(canvas, 1, new Color(0,0,0,200));
        }
        else if (masi.getScreenState() == 2) {
            helper.drawButton(canvas, 0, new Color(0,0,0,25));
            helper.drawButton(canvas, 1, new Color(0,0,0,200));
            helper.drawButtonBorder(canvas, 0, Color.WHITE);
        }
        else if (masi.getScreenState() == 3) {
            helper.drawButton(canvas, 0, new Color(0,0,0,200));
            helper.drawButton(canvas, 1, new Color(0,0,0,25));
            helper.drawButtonBorder(canvas, 1, Color.WHITE);
        }
        else if (masi.getScreenState() == 4) {
            helper.drawButton(canvas, 0, new Color(0,0,0,50));
            helper.drawButton(canvas, 1, new Color(0,0,0,50));
            helper.drawButtonBorder(canvas, 2, Color.YELLOW);
        }
        canvas.setFont(new Font("SansSerif", Font.BOLD, 100));
        canvas.setColor(Color.ORANGE);
        canvas.drawString("Speed Racer", 50, 140);
        canvas.setFont(new Font("SansSerif", Font.BOLD, 45));
        canvas.drawString("START", 790, 625);
        canvas.setColor(Color.WHITE);
        canvas.drawString("Name:", 50, 275);
        canvas.drawString(masi.getName(0), 75, 360);
        canvas.drawString("Car name:", 50, 475);
        canvas.drawString(masi.getName(1), 75, 560);  
    }

    public void game_over (Graphics canvas) {
        canvas.setColor(brg);
        canvas.fillRect(0, 0, this.getWidth(), this.getHeight());

        if (rollingText < 1440) {
            rollingText += 1;
            canvas.setColor(Color.ORANGE);
            canvas.setFont(new Font("SansSerif", Font.BOLD, rollingText/20 + 20));

            GraphicHelperMethods.drawCenteredString(canvas, "Gralla " + masi.getName(0) + ", with your car: " +masi.getName(1), 
                0, rollingText-400, this.getWidth(), this.getHeight());
    
            canvas.setFont(new Font("SansSerif", Font.BOLD, (rollingText-100)/20 + 20));
            GraphicHelperMethods.drawCenteredString(canvas, "Yeah, well done!", 
                0, rollingText-500, this.getWidth(), this.getHeight());
    
            canvas.setFont(new Font("SansSerif", Font.BOLD, (rollingText-200)/20 + 20));
            GraphicHelperMethods.drawCenteredString(canvas, "Again, congratulations, you won!", 
                0, rollingText-600, this.getWidth(), this.getHeight());
    
            canvas.setFont(new Font("SansSerif", Font.BOLD, (rollingText-400)/20 + 20));
            GraphicHelperMethods.drawCenteredString(canvas, "Created by: Ø", 
                0, rollingText-800, this.getWidth(), this.getHeight());
    
            canvas.setFont(new Font("SansSerif", Font.BOLD, (rollingText-500)/20 + 20));
            GraphicHelperMethods.drawCenteredString(canvas, "Game design, programming ...", 
                0, rollingText-900, this.getWidth(), this.getHeight());
    
            canvas.setFont(new Font("SansSerif", Font.BOLD, (rollingText-600)/20 + 20));
            GraphicHelperMethods.drawCenteredString(canvas, "and graphics: Ø", 
                0, rollingText-1000, this.getWidth(), this.getHeight());
    
            canvas.setFont(new Font("SansSerif", Font.BOLD, (rollingText-800)/20 + 20));
            GraphicHelperMethods.drawCenteredString(canvas, "RING DING DING DING DING DING!", 
                0, rollingText-1200, this.getWidth(), this.getHeight());

            canvas.setFont(new Font("SansSerif", Font.BOLD, 60));
            GraphicHelperMethods.drawCenteredString(canvas, "DU BIST WELTMEISTER!", 
                0, rollingText-1330, this.getWidth(), this.getHeight());
        }
        else {
            helper.drawButton(canvas, 11, Color.GREEN);
            helper.drawButton(canvas, 12, Color.RED);
            
            if (masi.getScreenState() == -11) {
                helper.drawButton(canvas, 11, new Color(0,0,0,50));
                helper.drawButtonBorder(canvas, 11, Color.BLUE);
            }
            else if (masi.getScreenState() == -12) {
                helper.drawButton(canvas, 12, new Color(0,0,0,50));
                helper.drawButtonBorder(canvas, 12, Color.BLUE);
            }

            canvas.setFont(new Font("SansSerif", Font.BOLD, 40));
            canvas.setColor(Color.BLUE);
            canvas.drawString("Restart", 325, 390);
            canvas.drawString("Quit", 650, 390);
            newRound = 1;
        }
    }

    @Override
    public Dimension getPreferredSize() {
        int width = 1100;
        int height = 750;
        return new Dimension(width, height);
    }
}
