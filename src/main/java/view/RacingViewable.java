package view;

import director.GameState;
import java.awt.Graphics;

public interface RacingViewable {

    /**
     * The game state is the general state of the game.
     * @return the current game state
     */
    public GameState getGameState();
    
    /**
     * @return the level of the game as an integer.
     */
    public int getCurrentLevel();

    /**
     * Sets the terrain.
     * @param rgb the color of the terrain to be set.
     */
    public void setCurrentTerrain(int rgb);

    /**
     * Draws the gates at the track
     * @param canvas the graphics to draw at
     */
    public void drawGates(Graphics canvas);

    /**
     * @param select the performance attribute, 1 for engine, 2 for aero, 3 for grip
     * @return the price of an upgrade of the selected attribute
     */
    public int getPrice(int select);

    /**
     * @return the balance of ones 'bank account'
     */
    public int getBal();

    /**
     * @return the amount one earned last run
     */
    public int getLastIncome();

    /**
     * @return the target lap time as a string
     */
    public String getTargetLapTime();

    /**
     * @return the name of the next series
     */
    public String getNextSeries();

    /**
     * @return the name of the current series
     */
    public String getCurrentSeries();

    /**
     * If you level up the message will have a congratulation, else
     * it will be blank
     * @return a message
     */
    public String getLevelUpMsg();

    /**
     * @param select the performance attribute, 1 for engine, 2 for aero, 3 for grip
     * @return the level of the selected attribute
     */
    public int getLevel(int select);

    /**
     * The screen state is a spesific state of the game underneath the game state.
     * @return the current screen state.
     */
    public int getScreenState();

    /**
     * @param select 0 for player name, 1 for car name
     * @return the name of the selected type
     */
    public String getName(int select);

    /**
     * @param i 0 for final lap time, 1 for current lap time
     * @return a string with the selected time
     */
    public String getLapTime(int i);

    /**
     * @return 1 if current terrain is grass or gravel, 0 else
     */
    public int grassOrGravel();

    /**
     * @return 1 if the car drifts, 0 else
     */
    public int doDrift();

    /**
     * @return 1 if checkpoints are visible, 0 else
     */
    public int getCheckpointStatus();

    /**
     * @return the current speed
     */
    public int getSpeed();

    /**
     * @return the current delta compared to target time
     */
    public String getDelta();

    /**
     * @return 1 if sounds are on, 0 else
     */
    public int getSound();

    /**
     * @return the x value of the car postion
     */
    public double getX();

    
    /**
     * @return the y value of the car postion
     */
    public double getY();

    /**
     * @return the angle of rotation the car has
     */
    public double getCarAngle();
    
}
