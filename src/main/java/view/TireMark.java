package view;

import java.awt.Color;
import java.util.ArrayList;

/**
 * A class controlling an arraylist having all tiremarks stored within.
 */
class TireMark {

    private final double x;
    private final double y;

    private final double width = 20;
    private final double height = 20;

    private final Color clr = new Color(0,0,0,50);

    private ArrayList<TireMark> marks = new ArrayList<>();

    TireMark() {
        x = 0;
        y = 0;
    }
    
    private TireMark (double x, double y) {
        this.x = x;
        this.y = y;
    }

    protected double getWidth() {
        return width;
    }

    protected double getHeight() {
        return height;
    }

    protected Color getColor() {
        return clr;
    }

    protected double getPositionX() {
        return x + 550;
    }

    protected double getPositionY() {
        return y + 375;
    }

    protected void newTireMark(double x, double y) {
        marks.add(new TireMark(x, y));
    }

    protected void clearMarks() {
        marks.clear();
    }

    protected ArrayList<TireMark> getTireMarks() {
        return marks;
    }
}
