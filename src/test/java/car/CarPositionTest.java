package car;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class CarPositionTest {
    
    @Test
    void basicCheck() {
        CarPosition tester = new CarPosition(100, 100);
        // tests the reset method
        tester.reset();
        assertTrue(tester.getX() == tester.getY() & tester.getX() == 0);
        // tests setPos and delta x and y
        tester.setPos(52, -52);
        tester.deltaX(-52);
        tester.deltaY(52);
        assertTrue(tester.getX() == tester.getY() & tester.getX() == 0);
        // set-get test for angle
        tester.setCurrentAngle(52);
        assertEquals(tester.getCurrentAngle(),52);
    }
}
