package car;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class CarVectorTest {
    
    @Test
    void basicCheck() {
        CarVector test1 = new CarVector(5,5);
        CarVector test2 = new CarVector(10, 10);

        // sanity check of getX and getY
        assertTrue(test1.getX()*2 == test2.getX());
        assertTrue(test2.getY()/2 == test1.getY());

        // set test
        test2.set(test1);
        assertTrue(test1.getX() == test2.getX());
        assertTrue(test1.getY() == test2.getY());

        // tests the scaling method
        test1.scaleVector(2);
        assertEquals(test1.getX(), 10);
        assertEquals(test1.getY(), 10);

        // tests the unit vector method
        test2.getUnitVector();
        assertEquals(Math.round(test2.getLength()*10000)/10000, 1);

        // delta x and y test
        test1.deltaX(-5);
        test1.deltaY(5);
        assertFalse(test1.getX() == test1.getY());
        assertEquals(test1.getX(), 5);
        
        // tests cos and sin direction methods
        test1.deltaX(5);
        test1.deltaY(-5);
        assertEquals(test1.cosDirection(), test1.sinDirection());

        // tests reset method
        test1.reset();
        test2.reset();
        assertTrue(test1.getX() == 0 & test1.getY() == -2 & test1.getX() == test2.getX() & test1.getY() == test2.getY());

        // tests the turning, both clockwise and counter clockwise
        test1.turn(Math.PI);
        assertEquals(Math.round(test1.cosDirection()*1000)/1000, Math.round(test2.cosDirection()*1000)/1000);
        assertEquals(Math.round(test1.sinDirection()*1000)/1000, -Math.round(test2.sinDirection()*1000)/1000);
        
        test1.turn(Math.PI);
        assertEquals(Math.round(test1.cosDirection()*1000)/1000, Math.round(test2.cosDirection()*1000)/1000);
        assertEquals(Math.round(test1.sinDirection()*1000)/1000, Math.round(test2.sinDirection()*1000)/1000);
        
        test1.turn(-Math.PI/2);
        assertEquals(Math.round(test1.cosDirection()*1000)/1000, Math.round(test2.sinDirection()*1000)/1000);
        assertEquals(Math.round(test1.sinDirection()*1000)/1000, Math.round(test2.cosDirection()*1000)/1000);
    }
}
