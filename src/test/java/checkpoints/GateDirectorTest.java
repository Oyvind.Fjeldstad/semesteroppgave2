package checkpoints;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class GateDirectorTest {
    
    GateDirector man = new GateDirector();

    @Test
    void basicCheck() {
        // checks that the number of gates is correct
        assertEquals(man.getNumberofGates(0), 17);
        assertEquals(man.getNumberofGates(1), 38);
        assertEquals(man.getNumberofGates(2), 41);

        // simple check that the checkgate checks for the correct number
        assertTrue(man.checkGate(2125, -900, 5, 2));
        assertFalse(man.checkGate(300, 400, 5, 2));
    }
}
