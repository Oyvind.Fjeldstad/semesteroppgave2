package checkpoints;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class GateTest {

    @Test
    void basicCheck() {
        Gate tester = Gate.tester;
        // tests the basics
        assertEquals(tester.getXPos(), 100);
        assertEquals(tester.getYPos(), 0);
        assertEquals(tester.getWidth(), 200);
        assertEquals(tester.getHeight(), 400);

        // tests the isWithin method
        assertTrue(tester.isWithin(100, 0));
        assertTrue(tester.isWithin(300, 400));
        assertTrue(tester.isWithin(200, 277));

        assertFalse(tester.isWithin(99, 0));
        assertFalse(tester.isWithin(300, 401));
        assertFalse(tester.isWithin(-100, -50));
    }
    
}
