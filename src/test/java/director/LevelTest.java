package director;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class LevelTest {

    @Test
    void basicCheck() {
        Level tester = Level.D;
        // basic get tests
        assertEquals(0, tester.getTarget());
        assertEquals("WELTMEISTER", tester.getName());
        assertEquals("0", tester.getLapTime());
    }
    
}
