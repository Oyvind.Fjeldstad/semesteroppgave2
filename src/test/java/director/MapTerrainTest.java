package director;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class MapTerrainTest {

    @Test
    void basicCheck() {
        MapTerrain tester = MapTerrain.TRACK;
        // tests the get methods
        assertTrue(tester.getColor() == -8421505);
        assertEquals(1, tester.getEngineCf());
    }
    
}
