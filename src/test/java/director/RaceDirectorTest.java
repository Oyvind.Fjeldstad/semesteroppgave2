package director;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import car.CarPosition;

public class RaceDirectorTest {

    CarPosition pos = new CarPosition(0, 0);
    RaceDirector masi = new RaceDirector(pos);

    @Test
    void setgetTests() {
        // terrain
        masi.setCurrentTerrain(MapTerrain.TRACK.getColor());
        assertFalse(1 == masi.grassOrGravel());
        masi.setCurrentTerrain(MapTerrain.GRAVEL.getColor());
        assertEquals(MapTerrain.GRAVEL, masi.getSurface());
        assertTrue(1 == masi.grassOrGravel());

        // game state
        masi.setActive();
        assertEquals(GameState.ACTIVE_GAME, masi.getGameState());

        // name
        masi.setName("ola", 0);
        masi.setName("nordmann", 1);
        assertTrue("ola" == masi.getName(0));
        assertTrue("nordmann" == masi.getName(1));

        // screen state
        masi.setScreenState(52);
        assertEquals(52, masi.getScreenState());

        // basic test of the shop system, see ShopTest.java for more
        assertTrue(masi.getBal() == 0);
        masi.income(2000);
        assertEquals(2000,masi.getLastIncome());
        assertTrue(masi.getBal() == 2000);
        masi.buy(1);
        assertFalse(masi.getBal()==2000);
        assertTrue(masi.getLevel(1)==2);

        // drift
        masi.drift(52);
        assertEquals(52,masi.doDrift());

        // checkpoint
        masi.setCheckpointStatus(-52);
        assertEquals(-52, masi.getCheckpointStatus());

        // speed
        masi.setSpeed(1);
        assertEquals(1*5, masi.getSpeed());

        // sound
        masi.setSound(7);
        assertTrue(7 == masi.getSound());
    }

    @Test
    void voidChecks() {
        masi.setScreenState(52);
        assertFalse(10 == masi.getScreenState());
        masi.crash();
        assertTrue(10 == masi.getScreenState());
        
        assertFalse(GameState.ACTIVE_GAME == masi.getGameState());
        masi.setActive();
        assertTrue(GameState.ACTIVE_GAME == masi.getGameState());

        pos.deltaX(10);
        pos.deltaY(10);
        
        assertTrue(pos.getX() == masi.getX());
        assertTrue(pos.getY() == masi.getY());
        assertTrue(pos.getCurrentAngle() == masi.getCarAngle());

        assertFalse(0 == pos.getX());
        assertFalse(0 == pos.getY());
        masi.reset();
        assertTrue(0 == pos.getX());
        assertTrue(0 == pos.getY());

        pos.deltaX(10);
        pos.deltaY(10);
        assertFalse(0 == pos.getX());
        assertFalse(0 == pos.getY());
        masi.restart();
        assertTrue(0 == pos.getX());
        assertTrue(0 == pos.getY());
    }
    
}
