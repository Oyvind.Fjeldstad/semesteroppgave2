package director;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class ShopTest {

    Shop shop = new Shop();

    @Test
    void basicCheck() {
        // tests income and balance
        assertEquals(0, shop.getBalance());
        shop.income(1000);
        assertEquals(1000, shop.getBalance());
        // tests upgrade where you can afford
        int oldPrice = 100;
        shop.buy(oldPrice, 1);
        assertEquals(1000-oldPrice, shop.getBalance());
        assertEquals(2, shop.getEngineLevel());
        // tests upgrade when you can not afford
        shop.buy(shop.getBalance()+1, 1);
        assertEquals(1000-oldPrice, shop.getBalance());
        assertEquals(2, shop.getEngineLevel());
        // tests restart
        shop.restart();
        assertEquals(0, shop.getBalance());
        assertEquals(1, shop.getEngineLevel());
    }
    
}
