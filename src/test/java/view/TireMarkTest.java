package view;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.Color;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class TireMarkTest {

    TireMark generator = new TireMark();

    @Test
    void basicCheck() {
        // tests adding a tire mark
        generator.newTireMark(40, 52);
        ArrayList<TireMark> marks = generator.getTireMarks();
        TireMark tester = marks.get(0);
        // checks that the tire mark was added correctly
        assertEquals(40, tester.getPositionX()-550);
        assertEquals(52, tester.getPositionY()-375);
        assertEquals(20, tester.getWidth());
        assertEquals(20, tester.getHeight());
        assertEquals(new Color(0,0,0,50).getRGB(), tester.getColor().getRGB());
        // tests the clearMarks method
        marks.clear();
        generator.clearMarks();
        assertTrue(marks == generator.getTireMarks());
    }
    
}
